Readme for my assignment 1:

1. Signed up for Bitbucket. 
2. Created a public repository with source code - sample.html.
3. Fill out the attached html link to this repository and submited it in the assignment folder.	
4. Added an issue tracker and create an issue of something that you didn't quite 
	get working or of an enhancement that you would like to make. 
	The link is https://bitbucket.org/Dsunilnooranal5738/repo/issues?status=new&status=open
5. Created a README.md file with instructions on how to install and use bit bucket and add the various steps in assignment 1
6. Added a license for your project and explained your choice in a note in the README.md file;
	https://bitbucket.org/Dsunilnooranal5738/repo/src/master/LICENSE
7. Added a project Wiki and wrote an article that explains why a CAPSTONE customer might want you 
	to use a private repository rather than a public one. Contrast the CAPSTONE customer's needs with the needs of Signal 		Private Messenger and describe why they would be open source. https://bitbucket.org/Dsunilnooranal5738/repo/wiki/Home
8. created a seperate project as well for the same: https://bitbucket.org/Dsunilnooranal5738/workspace/projects/WIKI
9. Made a changes to your README.md file, commit it and push to bitbucket.
